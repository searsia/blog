---
layout: post
title: Releasing a refactored client
bigimg: /img/searsia-wordcloud.png
share-img: https://blog/searsia.org/img/searsia-wordcloud.png
tags: [release]
---

We refactored our example client, [searsiaclient][1]! Yay!
Ok, this will be our nerdiest post in a while. In software design, 
[code refactoring][2] refers to restructuring the code without changing
its external behaviour. So, what's the big deal, if the clients behaviour
is still the same? Previously, searsiaclient not only depended on 
external libraries, such as [JQuery][3] and [Bootstrap][4], but those
dependencies were used everywhere in the [searsia.js][5] Javascript file.
Organisations that adopt Searsia therefore, had to include those external
libraries in their websites as well, even though they may not further 
need them. For that reason, the comments in the file contained the
terrible software swear "[Spaghetti code][10]"... Until today, that is. 

With the release of [searsiaclient version 1.2.0][8], we removed those
dependencies completely from [searsia.js][5], and we fixed the very
[first issue][6]: We implemented the [searsia.js][5] library following
the [closure pattern][7], which provides a sort of encapsulation for the
variables and functions inside the library. Long story short: You can 
savely include the [searsia.js][5] library in your website, and its
contents will not clash or overwrite variables and functions that your
site already uses (that may have the same names). Also, as stated above,
the library no longer depends on JQuery and Bootstrap, so there is not
need to include those.

The release still contains a full blown example client however, that
uses JQuery and Bootstrap for its example implementation, but all the 
presentation stuff is moved to [exampleclient.js][9]. 
Hurray!
 
[1]: https://codeberg.org/searsia/searsiaclient "Searsiaclient on Codeberg"
[2]: https://en.wikipedia.org/wiki/Code_refactoring "Code refactoring on Wikipedia"
[3]: https://jquery.com/ "JQuery"
[4]: https://getbootstrap.com/ "Bootstrap"
[5]: https://codeberg.org/searsia/searsiaclient/src/branch/main/js/searsia.js "searsia.js"
[6]: https://codeberg.org/searsia/searsiaclient/issues/1 "Issue 1: Closure"
[7]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#closures "Mozilla Javascript Guide: Functions, Closures"
[8]: https://codeberg.org/searsia/searsiaclient/releases/tag/v1.2.0 "Searsiaclient version 1.2.0"
[9]: https://codeberg.org/searsia/searsiaclient/src/branch/main/js/exampleclient.js "exampleclient.js"
[10]: https://en.wikipedia.org/wiki/Spaghetti_code "Spaghetti code on Wikipedia"
